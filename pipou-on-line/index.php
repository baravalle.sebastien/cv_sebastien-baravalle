<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RPG JS ET AJAX</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />

</head>
<body>
    
    <canvas height="480" width="320" id="canvas">
    
    </canvas>
</body>

<script type="text/javascript" src="js/classes/Tileset.js"></script>
<script type="text/javascript" src="js/classes/map.js"></script>
<script type="text/javascript" src="js/classes/Personnage.js"></script>

    <script src="js/main.js">
    </script>
</html>