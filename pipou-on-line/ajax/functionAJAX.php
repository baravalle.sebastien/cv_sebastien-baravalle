<?php
function getFileToJSON($fileName){
    $file= fopen($fileName,'a+');
    $fileText=fgets($file);
    $json= json_decode($fileText,true);
    return $json;
}
function putJSONinFile($JSON, $fileName){
    $json=json_encode($JSON);
    $newFile=fopen($fileName,'w+');
    fputs($newFile, $json);

}
function isInArray($array, $valueToFind){
    foreach($array as $v ){
        if($valueToFind == $v){
            return true;
        
        }
    }
    return false;
}
function putInArray($array, $valueToFind, $newPerso){
    $objetRetour = array();
    foreach($array as $k => $v ){
        if($array[$k]['name'] == $valueToFind ){
            unset($array[$k]);

            $objetJoueur = (object)array(
                "x" => $newPerso['x'], 
                "y" => $newPerso['y'],
                "direction" =>  $newPerso['direction'],
                "name" =>  $newPerso['name'],
                "mvt" =>$newPerso['mvt']
            );

        }else{

            $objetJoueur = (object)array(
                "x" => $array[$k]['x'], 
                "y" => $array[$k]['y'],
                "direction" =>  $array[$k]['direction'],
                "name" =>  $array[$k]['name'],
                "mvt" =>$array[$k]['mvt']

            );
        }
        $objetRetour[] = $objetJoueur;
    }
    return $objetRetour;

}
// si plus de deux connectés on supprime tout.
function deleteGamer( $repertoire){
    $gamerNumber = count($repertoire);
    if($gamerNumber>2){
        foreach($repertoire as $fichier){
            unlink($fichier);
        }
        unlink('../JSON/repertoire.json');
    }
}