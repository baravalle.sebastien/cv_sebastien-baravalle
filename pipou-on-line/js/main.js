
var map = new Map("map");
let tokenJS = Math.random().toString(20).substr(2, 6);

window.onload = function(){
    let canvas = document.querySelector('#canvas');
    let context = canvas.getContext('2d');

    canvas.width  = map.getLargeur() * 32;
	canvas.height = map.getHauteur() * 32;
	
    let joueur = new Personnage( 9, 9, DIRECTION.BAS, tokenJS);

    // lorsqu'on ajoute un personnage on fait un objet 
    //  ajax visible pour tout les joueur
    // puis lorsqu'on déplace on écrit cet objet,
    // lorsqu'on dessine on dessine ces objet

     map.addPersonnage(joueur);
    setInterval(function() {

        map.dessinerMap(context);
    }, 40);
    setInterval(function() {
        map.actualisePersonnages(joueur);
    }, 160 );
    setInterval(function() {

        map.recuperation();
    }, 960 );
    // Gestion du clavier
    window.onkeydown = function(e){
        return false;
    }
    window.onkeyup = function(event) {

        var e = event || window.event;
        var key = e.which || e.keyCode;

        switch(key) {

            case 38 : 
                joueur.deplacer(DIRECTION.HAUT, map.terrain);
                
                break;
            case 40 : 
                joueur.deplacer(DIRECTION.BAS, map.terrain);

                break;
            case 37 : 
                joueur.deplacer(DIRECTION.GAUCHE, map.terrain);
                break;
            case 39 :
                joueur.deplacer(DIRECTION.DROITE, map.terrain);
                break;

            
            case 36 : 
                joueur.deplacer(DIRECTION.HG, map.terrain);
                break;
            case 33 : 
                joueur.deplacer(DIRECTION.HD, map.terrain);
                break;
            case 34 :
                joueur.deplacer(DIRECTION.BD, map.terrain);
                break;   
            case 35 : 
                joueur.deplacer(DIRECTION.BG, map.terrain);
                break;
            default : 
               
                return true;
        }
            map.sendAjax(joueur);

        return false;
    }



}