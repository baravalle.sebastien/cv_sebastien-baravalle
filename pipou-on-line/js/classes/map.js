function Map(nom) {

    this.personnages = new Array();

	var xhr = new XMLHttpRequest();

    // Chargement du fichier
    xhr.open("GET", './maps/' + nom + '.json', false);
    xhr.send(null);
    
        var mapJsonData = xhr.responseText;
        var mapData = JSON.parse(mapJsonData);

        this.tileset = new Tileset(mapData.tileset);
        this.terrain = mapData.terrain;


    // Chargement de l'image du joueur
	this.image = new Image();
	this.image.onload = function() {
		
	}
    this.image.src = "sprites/PipouSprite.png" ;


}

Map.prototype.getHauteur = function() {
    return this.terrain.length;
}
Map.prototype.getLargeur = function() {
    return this.terrain[0].length;
}



// Pour ajouter un personnage
Map.prototype.addPersonnage = function(perso) {
    xhr = new XMLHttpRequest();
    xhr.open("GET", './joueurs/joueurs.json',false);
    xhr.send(null);
    
    var joueursJsonData = xhr.responseText;

    if(joueursJsonData){
        
        var joueursData = JSON.parse(joueursJsonData);
        
        if(joueursData.length > 3){
            
            joueursData = new Array();
        }
        // il y a d'autre personnages sur la liste
        // alors on les ajoute a la liste d'objet personne: 
        
        
        for(i=0; i<joueursData.length; i++ ){
            newPerso = new Personnage(joueursData[i]['x'],joueursData[i]['y'],joueursData[i]['direction'],joueursData[i]['name']  );
            this.personnages.push(newPerso);
        }
    

    }else{

        joueursData = new Array();

    } 
   
    this.personnages.push(perso);

    joueursData.push(perso);   

    //retourner liste de tout les perso au serveur.
    
    let json = JSON.stringify(joueursData);
    xhr = new XMLHttpRequest();
    
    // envoi des coordones du Joueur
    xhr.open("GET", './ajax/ajax.php?liste='+json);
    xhr.send(null);
    
    console.log(' PROBLEME : le JSON finit par disparaitre, () du coup les perso connectés se perdent');
    console.log('Probleme les liste doivent être indexée de la meme manière pour faciliter les transfert entre liste objet et liste');
   
}
// envoie les coordonnes au serveur:
Map.prototype.sendAjax = function(perso) {
    
    perso.mvt= Math.random().toString(20).substr(2, 6);
    let json = JSON.stringify(perso);
    xhr = new XMLHttpRequest();
    // envoi des coordones du Joueur
    xhr.open("GET", './ajax/ajax.php?joueur='+json);
    xhr.send(null);
    

}
Map.prototype.reconstituerListe = function(){
    // reconstituer liste objet client et serveur

    let json = JSON.stringify(this.personnages);
    xhr = new XMLHttpRequest();
    
    // envoi des coordones du Joueur
    xhr.open("GET", './ajax/ajax.php?liste='+json);
    xhr.send(null);

}
Map.prototype.actualisePersonnages = function(perso){
    xhr = new XMLHttpRequest();
    xhr.open("GET", './joueurs/joueurs.json',false);
    xhr.send(null);
    try{
        var joueursData = JSON.parse(xhr.responseText);

    }catch{
        this.reconstituerListe(perso);

    }
    if(joueursData){


        for(i=0; i<joueursData.length; i++ ){
            if(perso.name != joueursData[i]['name']){
            
                for(ii=0; ii<this.personnages.length; ii++ ){
                    if(perso.name != this.personnages[ii]['name']){
                        if(this.personnages[i]['name'] == joueursData[i]['name']){
                            if(this.personnages[i]['mvt'] != joueursData[i]['mvt'])

                            this.personnages[i].deplacer( joueursData[i]['direction'], this.terrain);
                            this.personnages[i]['mvt'] = joueursData[i]['mvt'];

                        }

                    }
                }
            }
        }
    }

}
Map.prototype.recuperation = function(){
    xhr = new XMLHttpRequest();
    xhr.open("GET", './joueurs/joueurs.json',false);
    xhr.send(null);
    var joueursData = JSON.parse(xhr.responseText);
    for(i=0; i<joueursData.length; i++ ){
        if(!this.personnages[i]){
            newPerso = new Personnage(joueursData[i]['x'],joueursData[i]['y'],joueursData[i]['direction'],joueursData[i]['name'] );
            this.personnages.push(newPerso);
        }else{
            for(ii=0; ii<this.personnages.length; ii++ ){
                if(this.personnages[ii]['name'] == joueursData[i]['name']){

                    this.personnages[ii]['x'] = joueursData[i]['x'];
                    this.personnages[ii]['y'] = joueursData[i]['y'];
                }
            }

        }

    }

}

Map.prototype.dessinerMap = function(context) {
	for(var i = 0, l = this.terrain.length ; i < l ; i++) {
		var ligne = this.terrain[i];
		var y = i * 32;
		for(var j = 0, k = ligne.length ; j < k ; j++) {
            this.tileset.dessineTile(ligne[j], context, j * 32, y);
            

        }
        
        
    }
 
    for(var i = 0, l = this.personnages.length ; i < l ; i++) {
    

        this.personnages[i].dessinerPersonnage(context, this.image);
        
    }   


}

