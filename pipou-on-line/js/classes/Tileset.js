function Tileset(url){
    this.image = new Image();
    this.image.reference = this;
    this.image.src = url;
    this.image.onload = function(){
        this.reference.largeur = this.width /32;

    }
}

Tileset.prototype.dessineTile = function(numero, context, xDestination, yDestination){


    // COUPE LE TILSET EN TRANCHE

    let xSourceTiles = numero %this.largeur;
    if(xSourceTiles == 0 ) xSourceTiles = this.largeur;

    let ySourceTiles = Math.ceil(numero / this.largeur);

    let xSource= (xSourceTiles-1)*32;
    let ySource= (ySourceTiles-1)*32;




    context.drawImage(this.image, xSource, ySource,32,32, xDestination, yDestination,32,32);

}