var DIRECTION = {
	"BAS"    : 0,
	"GAUCHE" : 1,
	"DROITE" : 2,
    "HAUT"   : 3,
    "HG": 4,
    "HD":5,
    "BD":6,
    "BG":7
}
var DUREE_ANIMATION = 2;
var DUREE_DEPLACEMENT = 5;

// Ici se trouvera la classe Personnage

function Personnage(x, y, direction, name) {
	this.x = x; // (en cases)
	this.y = y; // (en cases)
	this.direction = direction;
	this.mvt = false;
	
    
  
    this.etatAnimation = -1;
    this.name = name;
}

Personnage.prototype.dessinerPersonnage = function(context, image) {

    var frame = 0;
    var decalageX = 0, decalageY = 0;
    if(this.etatAnimation >= DUREE_DEPLACEMENT) {
        // Si le déplacement a atteint ou dépassé le temps nécessaire pour s'effectuer, on le termine
        this.etatAnimation = -1;
    } else if(this.etatAnimation >= 0) {
        // On calcule l'image (frame) de l'animation à afficher
        frame = Math.floor(this.etatAnimation / DUREE_ANIMATION);
        if(frame > 3) {
            frame %= 4;
        }
	
        // Nombre de pixels restant à parcourir entre les deux cases
        var pixelsAParcourir = 32 - (32 * (this.etatAnimation / DUREE_DEPLACEMENT));
        
        // À partir de ce nombre, on définit le décalage en x et y.
        // NOTE : Si vous connaissez une manière plus élégante que ces quatre conditions, je suis preneur
        if(this.direction == DIRECTION.HAUT) {
            decalageY = pixelsAParcourir;
        } else if(this.direction == DIRECTION.BAS) {
            decalageY = -pixelsAParcourir;
        } else if(this.direction == DIRECTION.GAUCHE) {
            decalageX = pixelsAParcourir;
        } else if(this.direction == DIRECTION.DROITE) {
            decalageX = -pixelsAParcourir;
        }else if(this.direction == DIRECTION.HD) {
            decalageY = pixelsAParcourir;
            decalageX = -pixelsAParcourir;

        } else if(this.direction == DIRECTION.HG) {
            decalageY = pixelsAParcourir;
            decalageX = pixelsAParcourir;

        } else if(this.direction == DIRECTION.BD) {
            decalageY = -pixelsAParcourir;
            decalageX = -pixelsAParcourir;

        } else if(this.direction == DIRECTION.BG) {
            decalageY = -pixelsAParcourir;
            decalageX = pixelsAParcourir;

        }
        
        this.etatAnimation++;
    }
/*
 * Si aucune des deux conditions n'est vraie, c'est qu'on est immobile, 
 * donc il nous suffit de garder les valeurs 0 pour les variables 
 * frame, decalageX et decalageY
 */

    imagePosition = this.direction;
    if(imagePosition>3){
        DUREE_DEPLACEMENT = 8;

        imagePosition = imagePosition == 4 ?1 : imagePosition;
        imagePosition = imagePosition == 5 ? 2 : imagePosition;
        imagePosition = imagePosition == 6 ? 2: imagePosition;
        imagePosition = imagePosition == 7 ? 1: imagePosition;
    
    }
    context.drawImage(
        
        image, 
        32 * frame, imagePosition * 32, // Point d'origine du rectangle source à prendre dans notre image
        32, 32, // Taille du rectangle source (c'est la taille du personnage)
        (this.x * 32) - (32 / 2) + 16+decalageX, (this.y * 32) - 32 + 24+decalageY, // Point de destination (dépend de la taille du personnage)
        32, 32 // Taille du rectangle destination (c'est la taille du personnage)
    );
    

}
Personnage.prototype.getCoordonneesAdjacentes = function(direction)  {
	var coord = {'x' : this.x, 'y' : this.y};
	switch(direction) {
		case DIRECTION.BAS : 
			coord.y++;
			break;
		case DIRECTION.GAUCHE : 
			coord.x--;
			break;
		case DIRECTION.DROITE : 
			coord.x++;
			break;
		case DIRECTION.HAUT : 
			coord.y--;
            break;
        case DIRECTION.HG : 
            coord.y--;
            coord.x--;
			break;
		case DIRECTION.HD :
            coord.x++; 
			coord.y--;
			break;
		case DIRECTION.BD : 
            coord.x++;
            coord.y ++;
			break;
        case DIRECTION.BG : 
            coord.x--;
			coord.y++;
            break;
	}
	return coord;
}

	
Personnage.prototype.deplacer = function(direction, terrain) {
    
    // On ne peut pas se déplacer si un mouvement est déjà en cours !
    if(this.etatAnimation >= 0) {
        return false;
    }
    DUREE_DEPLACEMENT = 5;

	// On change la direction du personnage
	this.direction = direction;
		
	// On vérifie que la case demandée est bien située dans la carte
    var prochaineCase = this.getCoordonneesAdjacentes(direction);
    

    if(terrain[prochaineCase.y][prochaineCase.x] == 30) {
		// On retourne un booléen indiquant que le déplacement ne s'est pas fait, 
		// Ça ne coute pas cher et ca peut toujours servir
		return false;
	}
		
	// On effectue le déplacement
	this.x = prochaineCase.x;
	this.y = prochaineCase.y;
	this.etatAnimation = 1;	
	return true;
}
